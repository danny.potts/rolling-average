#include "RollingAverage.h"

RollingAverage<uint16_t, 5> average;

void setup() {
  Serial.begin(115200);
  Serial.println("Booting...");
  delay(1);
}

void loop() {

  uint16_t reading = analogRead(A7);
  average.addRecord(reading);

  Serial.print("Reading: ");
  Serial.println(reading);

  Serial.print("Average: ");
  Serial.println(average.getAverage());

  Serial.println("");

  delay(100);
}
