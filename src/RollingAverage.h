/*
  Danny's rolling average C++ library
  Version 1
  Header

  D C Potts 2019
*/

#ifndef ROLLINGAVERAGE_H
#define ROLLINGAVERAGE_H

#include "stdint.h"

template <class T, const unsigned long length>
class RollingAverage
{
  static const unsigned long _capacity = length;

public:

  RollingAverage(){};
  ~RollingAverage(){};

  double getAverage() {
    if (_noOfRecords == 0) {
      return 0;
    }

    double output = 0;

    for (unsigned long i = 0; i < _noOfRecords; i++) {
      output+= _records[i];
    }
    output = output/_noOfRecords;
    return output;
  }

  void addRecord(T record)
  {
    _records[_recordIndex] = record;

    _recordIndex++;
    _noOfRecords++;

    if (_recordIndex >= _totalNoOfRecords) {
      _recordIndex = 0;
    }

    if (_noOfRecords > _totalNoOfRecords) {
      _noOfRecords = _totalNoOfRecords;
    }

    return;
  };

private:

  T _records[_capacity] = {};
  unsigned long _recordIndex = 0;
  unsigned long _noOfRecords = 0;
  unsigned long _totalNoOfRecords = _capacity;

};

#endif
